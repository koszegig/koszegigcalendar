import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy} from '@angular/core';
import { AlertService} from '../../../base-elements/services/alert.service';
import { CurrentUserService} from '../../../services/custom/current-user.service';
import { DomSanitizer } from '@angular/platform-browser';
import { RootClass } from '../../../models/RootClass';


@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})

export class AdminSuperRootComponent extends RootClass implements OnInit, AfterViewInit {
  protected alertService: AlertService;
  protected sanitizer: DomSanitizer;
  protected currentUserService: CurrentUserService;
  protected cdRef: ChangeDetectorRef;

  public domid = null;
  public menu_id;
  public menu_title;
  public table_name;

  public FormComponent: any;
  public loadingGIF;
  public loading = false;
  public toastr = false;
  protected isModal = true;

  constructor(){
    super();
    this.loadingGIF = this.environment.loadingGIF;
    try {
      this.sanitizer = this.injector.get(DomSanitizer);
      this.alertService = this.injector.get(AlertService);
    } catch (e) {
    }
  }

  ngOnInit() {}

  ngAfterViewInit(){}

  subscribeCurrentUser(){
    this.subscription = this.currentUserService.getEmittedValue()
    .subscribe(item => this.setCurrentUser(item));
  }

  setCurrentUser(item: any) {
     this.currentUser = item;
  }

  setAlertService(alert: any){
    if (alert.status == 'success')
      this.alertService.success(alert.message, true);
    else
      this.alertService.error(alert.message);
  }

  sanitize(url: string) {
    if  (url == '') return '';
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }

  clickevent(params: any) {
    const methodName = this.__.get(params, 'method');
    const fnParams = this.__.get(params, 'params');
    if (this[methodName]) {
      this[methodName](...fnParams); // call it
    }
  }

  isButtonDisplay(btn: any, params: any, row: any = []){
    if (this._.isUndefined(btn)) return false;
    let display = true;
    const modprop = this.__.get(btn, 'modprop', null);
    ////this.logging(display, 'display', 'isButtonDisplay');
    if (!this._.isNull(modprop)){
     // //this.logging(modprop, 'modprop', 'isButtonDisplay');
      display = display && params.domelem.isDisplay(modprop);
    }
      /*//this.logging(display, 'display', 'isButtonDisplay');*/
      //this.logging(btn, 'btn', 'isButtonDisplay');
      //this.logging(row, 'row', 'isButtonDisplay',true);
    //  display = display && (row.bizf_feldolg_status_kod == 0);
      display = display && btn.isDisplay(row);
    return display;
  }
  isButtonShowDisplay(btn: any, params: any, row: any = []){
    let display = true;
    return display;
  }
  promise(action, prop: string, callback, sprop = 'data.dmdata') {
    ////this.logging(callback,  'callback' , 'listDn');
    const promise = this._promise(action, sprop);
    promise.then((val) => {
      ////this.logging(val, 'val', 'promise');
      if (!this._.isNull(prop))
        this[prop] = val;
        callback(val);
    });
  }

  _promise(action, prop = 'data.dmdata') {
    const self = this;
    return new Promise((resolve, reject) => {
      action.subscribe(
          data => {
            self.logging(data, 'data', '_promise');
            resolve(self.__.get(data, prop));
          },
          error => {
            reject(error);
          });
    }).catch(e => {
    });
  }

  async promiseAsync(action, prop: string, sprop = 'data') {
    const result = await action.toPromise();
    //this.logging(prop, 'prop', 'AdminSuperRootComponent.promiseAsync');
    if (!this._.isNull(prop)){
      this[prop] = this.__.get(result, sprop);
      //this.logging(this[prop], 'this[prop]', 'AdminSuperRootComponent.promiseAsync');
      return true;
    }
    else return this.__.get(result, sprop);
  }

  detectChanges(){
    if (!this._.isUndefined(this.cdRef))
    this.cdRef.detectChanges();
  }

  markForCheck(){
    //this.logging(this.cdRef, 'this.cdRef', 'markForCheck');
    if (!this._.isUndefined(this.cdRef))
    this.cdRef.markForCheck();
  }

  detach(){
    if (!this._.isUndefined(this.cdRef))
    this.cdRef.detach();
  }

  checkNoChanges(){
    if (!this._.isUndefined(this.cdRef))
    this.cdRef.checkNoChanges();
  }

  reattach(){
    if (!this._.isUndefined(this.cdRef))
    this.cdRef.reattach();
  }

}
