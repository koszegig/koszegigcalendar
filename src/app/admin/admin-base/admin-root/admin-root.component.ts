import { Component, Output, EventEmitter } from '@angular/core';
import { AdminSuperRootComponent } from '../admin-super-root/admin-super-root.component';
import { ToastrService } from "ngx-toastr";

@Component({
  template: ''
})

export class AdminRootComponent extends AdminSuperRootComponent {
  @Output() refreshTableRowEmmitter = new EventEmitter<any>();
  @Output() loadingIndicatorEmmitter = new EventEmitter<boolean>();
  @Output() openModalEmmitter = new EventEmitter<any>();
  @Output() closeModalEmmitter = new EventEmitter<any>();
  @Output() selectedEmmitter = new EventEmitter<any>();

  constructor() {
    super();
  }

  openModal(){
    ////this.logging(this.openModalEmmitter, 'this.openModalEmmitter', 'AdminRootComponent-openModal');
    if (!this._.isUndefined(this.openModalEmmitter))
      this.openModalEmmitter.emit();
  }

  closeModal(){
    if (!this._.isUndefined(this.closeModalEmmitter))
      this.closeModalEmmitter.emit();
  }

  refreshTableRowEmitter(){
    if (!this._.isUndefined(this.refreshTableRowEmmitter))
      this.refreshTableRowEmmitter.emit();
  }

  loadingIndicatorEmitter(status){
    if (!this._.isUndefined(this.loadingIndicatorEmmitter))
      this.loadingIndicatorEmmitter.emit(status);
  }

  getActionText(object){
    const action = this._.isEmpty(object) ? 'add' : 'update';
    return this.translateWParams('common', action);
  }
  getselectedEmmittert(object){
    if (!this._.isUndefined(this.selectedEmmitter))
    this.selectedEmmitter.emit();
  }
}
