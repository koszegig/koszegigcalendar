import { Component, OnInit, ChangeDetectorRef, OnDestroy, ViewChild, AfterViewInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { NgxCalendarComponent } from "ss-ngx-calendar";
import { sampleOne, sampleTwo } from '../../sample/sample-data';
declare var require: any;
import { SidebarService } from "../../services/builtin/sidebar.service";
import * as _ from "underscore";

import { NgxTuiCalendarComponent } from 'ngx-tui-calendar';
import { BeforeCreateScheduleEvent } from 'ngx-tui-calendar/lib/Models/Events';
import { Schedule } from 'ngx-tui-calendar/lib/Models/Schedule';
import { MonthOptions } from 'ngx-tui-calendar/lib/Models/MonthOptions';
import { WeekOptions } from 'ngx-tui-calendar/lib/Models/WeekOptions';
import { FullCalendarOptions, EventObject } from 'ngx-fullcalendar';

import { DayComponent } from '../../base-elements/dircetives/day/day.component';
import { WeekComponent } from '../../base-elements/dircetives/week/week.component';
import { MonthComponent } from '../../base-elements/dircetives/month/month.component';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarEventTimesChangedEvent,
  CalendarView,
} from 'angular-calendar';

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.css"]
})
export class IndexComponent implements  OnInit, OnDestroy {
  public isResizing: boolean = true;
  public sidebarVisible: boolean = true;
  public interval: any = {};

  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;

  calendarValue = null;
  calendarRange = null;
  calendarOptions3 = {
    isWeek: true,
    outline: false,
    isWithTime: true,
    evenDayDimensions: true,
    fromHour: 7, toHour: 19, hourInterval: 2, minuteInterval: 30

  };
  calendarOptions2 = {
    isWeek: true
  };
  events = sampleOne;
  options1 = {
    outline: false
  };
  options2 = {
    outline: false,
    evenDayDimensions: true
  };
  options: FullCalendarOptions;
  events1: EventObject[];
  @ViewChild('calendar', { static: true }) calendar: NgxTuiCalendarComponent;

  calendarViews = [
    { value: '0', name: 'month' },
    { value: '1', name: 'week' },
    { value: '2', name: 'day' },
  ];
  schedules: Schedule[] = [];
  defaultView = 'month';

  month: MonthOptions = {
    daynames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
    startDayOfWeek: 1,
    narrowWeekend: true,
  };


  constructor(
    private activatedRoute: ActivatedRoute,
    private sidebarService: SidebarService,
    private cdr: ChangeDetectorRef
  ) {
  }

  ngOnInit() {
   // this.calendar.changeView(this.defaultView);
    this.schedules.push( {
      id: '1',
      calendarId: '1',
      title: 'my schedule',
      category: 'time',
      dueDateClass: '',
      start: (new Date()),
      end: (new Date())
    });


    this.options = {
      defaultDate: '2018-07-26',
      editable: true,
    };

    this.events1 = [
      { id: 'a', title: 'My Birthday', allDay: true },
      { id: 'b', title: 'Friends coming round', start: '2018-07-26T18:00:00', end: '2018-07-26T23:00:00' }
    ];


    this.setView(CalendarView.Month);
  }

  ngOnDestroy() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }
  onChooseDate(date: any) {
    this.calendarValue = date;
  }
  onChangeDate(date: any) {
    this.calendarRange = date;
  }

  addDate() {
    this.events = sampleTwo;
  }
  setView(view: CalendarView) {
    this.view = view;
  }
}

