import { NgModule, ApplicationModule, Injector  } from '@angular/core';
import { AppInjector } from './../services/custom/app.injector.service';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { routing } from './admin.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalService } from './../services/custom/modal.service';
import { BaseElementsModule} from '../base-elements/base-elements.module';
import { NgxCalendarModule } from "ss-ngx-calendar";
import { AdminComponent } from './admin/admin.component';
import { RouterModule } from '@angular/router';
import { LayoutModule } from '../layout/layout.module';
import { NgxTuiCalendarModule } from 'ngx-tui-calendar';
import { NgxFullCalendarModule } from 'ngx-fullcalendar';
import { SchedulerModule } from 'angular-calendar-scheduler';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  imports: [
    routing,
    LayoutModule,
    BaseElementsModule,
    NgbModule,
    NgxCalendarModule,
    RouterModule,
    NgxTuiCalendarModule.forRoot(),
    NgxFullCalendarModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    SchedulerModule.forRoot({locale: 'hu', headerDateFormat: 'daysRange'}),
    CommonModule,
  ],
	declarations: [
		AdminComponent,
		IndexComponent,
  ],
  providers: [
    ModalService
  ]
})
export class AdminModule {
	constructor(injector: Injector){
		AppInjector.setInjector(injector);
	}
}
