import { environment } from '../../environments/environment';

export class LogHelper {

  static logging(object: any, pname: string = '', caller: string = 'logging',isdeb: boolean = false){
    if (environment.islogging){
      console.log('Class: ' + this.constructor.name);
      console.log('Caller: ' + caller);
      console.log('Property: ' + pname);
      console.log('Value:');
      console.log(object);
    }
  }
}
