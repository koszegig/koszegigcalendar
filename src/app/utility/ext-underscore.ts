import * as _ from 'underscore';
export class ExtUnderscore {

  static pluck(object, props){
      return _.map(object, function(item){
        return _.pick(item, props);
      });
    }

  static filterchecked(object){
    return _.filter(object, function(item: any){ return item.checked; });
  }

  static inArray(array, needle){
    return _.indexOf(array, needle) != -1;
  }

  static deepIndexOf(arr, obj) {
    return arr.findIndex(function (cur) {
      return Object.keys(obj).every(function (key) {
        return obj[key] === cur[key];
      });
    });
  }
}
