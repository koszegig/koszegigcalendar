import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { LeftsidebarComponent } from './leftsidebar/leftsidebar.component';
import { RightsidebarComponent } from './rightsidebar/rightsidebar.component';

@NgModule({
  declarations: [HeaderComponent,  PageLoaderComponent, LeftsidebarComponent, RightsidebarComponent],
	imports: [
    CommonModule,
		NgbModule,
		RouterModule
	],

	exports: [HeaderComponent, LeftsidebarComponent,RightsidebarComponent, PageLoaderComponent]
})
export class LayoutModule { }
