import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ThemeService } from '../../services/builtin/theme.service';
import { LogHelper } from '../../utility/log_helper';
import * as _ from 'underscore';

@Component({
  selector: 'app-leftsidebar',
  templateUrl: './leftsidebar.component.html',
  styleUrls: ['./leftsidebar.component.scss']
})
export class LeftsidebarComponent implements OnInit {
  @Input() sidebarVisible: boolean = true;
  @Input() navTab: string = 'menu';
  @Input() currentActiveMenu;
  @Input() currentActiveSubMenu;
  @Output() changeNavTabEvent = new EventEmitter();
  @Output() activeInactiveMenuEvent = new EventEmitter();
  public themeClass: string = 'theme-cyan';
  menus =[];
  public _menus =[
    {
      menu_id: 1,
      menu_action: "dashboard",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 10,
      menu_title: "Dashboard"
    },{
      menu_id: 2,
      menu_action: "scheduler",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 20,
      menu_title: "Scheduler"
    },{
      menu_id: 3,
      menu_action: "Search",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 30,
      menu_title: "Search By Data"
    },{
      menu_id: 4,
      menu_action: "pricing",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 40,
      menu_title: "Pricing"
    },{
      menu_id: 5,
      menu_action: "terms",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 50,
      menu_title: "Terms & Policies"
    },{
      menu_id: 6,
      menu_action: "about",
      menu_gyokersorrend: 10,
      menu_parentid: 0,
      menu_sorrend: 60,
      menu_title: "About Us"
    },
    {
      id: 6,
      menu_action: "Bookmark",
      menu_gyoker: 2,
      menu_gyokersorrend: 20,
      menu_hlevel: 0,
      menu_id: 3,
      menu_parentid: 1,
      menu_shortcut: null,
      menu_sorrend: 10,
      menu_title: "Bookmark"
    },

  ];
  constructor(private themeService: ThemeService,
              private router: Router) {
    this.themeService.themeClassChange.subscribe(themeClass => {
      this.themeClass = themeClass;
    });
  }

  ngOnInit(): void {
    this.menus = this.initialmenu(this._menus);
  }
  changeNavTab(tab: string) {
    this.navTab = tab;

  }

  activeInactiveMenu(menuItem: string) {
    //LogHelper.logging(menuItem, 'menuItem', 'activeInactiveMenu');
    this.activeInactiveMenuEvent.emit({ 'item': menuItem });
  }

  changeTheme(theme: string) {
    this.themeService.themeChange(theme);
  }
  initialmenu(menus) {
    LogHelper.logging(menus, 'menus', 'initialmenu');
    if (_.isEmpty(menus)) return [];
    const _menus = {};
    const _sortMenus = [];
    _.each(menus, function (menu: any) {
      if (menu.menu_parentid == 0) {
        menu.children = [];
        _menus[menu.menu_id] = menu;
        LogHelper.logging(menu, 'menu', 'initialmenu');
        if (menu.menu_sorrend > 0) {
          _sortMenus[menu.menu_sorrend] = menu.menu_id;
        } else {
          _sortMenus[menu.menu_id] = menu.menu_id;
        }
      }
      else {
        _menus[menu.menu_parentid].children.push(menu);


      }
    });
    //  LogHelper.logging(_menus , '_menus ', 'initialmenu');
    const _menutemps = {};
    _sortMenus.forEach(function (_sortMenu, index) {
      _sortMenus[index] = _menus[_sortMenu];
    });
       LogHelper.logging(_sortMenus , '_sortMenus ', 'initialmenu');
    return _.values(_sortMenus);

  }
  generateCustomLink(menu_action, menu_id, menu_title) {
    localStorage.setItem('menu_id', menu_id);
    localStorage.setItem('menu_title', menu_title);
    this.router.navigate(['/admin/' + menu_action + '/' + menu_id + '/' + menu_title]);
    //this.router.navigate(['/admin/frmCikk/8/Cikktörzs']);
  }




}
