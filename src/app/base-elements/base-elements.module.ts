
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {MultiTranslateHttpLoader} from 'ngx-translate-multi-http-loader';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import { SchedulerModule } from 'angular-calendar-scheduler';
//
import { DayComponent} from './dircetives/index';
//
import { HttpClientModule, HttpClient } from '@angular/common/http';
import param from './param.json';
import {CalendarModule} from 'angular-calendar';
import { WeekComponent } from './dircetives/week/week.component';
import { MonthComponent } from './dircetives/month/month.component';
export function createTranslateLoader(http: HttpClient) {
  return new MultiTranslateHttpLoader(http, param.lang_files);
}
@NgModule({
	declarations: [
    DayComponent,
    WeekComponent,
    MonthComponent
  ],
  imports: [
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    SchedulerModule.forRoot({locale: 'hu', headerDateFormat: 'daysRange'}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    HttpClientModule,
    CalendarModule,
  ],
	exports: [
    DayComponent,
    WeekComponent,
    MonthComponent,
    TranslateModule,
  ]
})

export class BaseElementsModule { }
