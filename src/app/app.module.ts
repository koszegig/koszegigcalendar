import { NgModule , Injector  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppInjector } from './services/custom/app.injector.service';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxCalendarModule } from "ss-ngx-calendar";
import * as $ from 'jquery';
import { AppRoutingModule } from './app-routing';

import { NgxTuiCalendarModule } from 'ngx-tui-calendar';
import { NgxFullCalendarModule } from 'ngx-fullcalendar';

import { SchedulerModule } from 'angular-calendar-scheduler';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NgxCalendarModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    SchedulerModule.forRoot({ locale: 'hu', headerDateFormat: 'daysRange' }),
    NgxTuiCalendarModule.forRoot(),
    NgxFullCalendarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(injector: Injector){
    AppInjector.setInjector(injector);
  }
}
