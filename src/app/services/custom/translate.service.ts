import { Inject } from '@angular/core';
import { TRANSLATIONS } from '../../translate/translations'; // import our opaque token

export class TranslateService {

    public _currentLang: string = 'hu';

    public get currentLang() {
        return this._currentLang;
    }

    // inject our translations
    constructor(@Inject(TRANSLATIONS) public _translations: any) {
    }

    use(lang: string): void {
        // set current language
        this._currentLang = lang;
    }

    translate(modul: string, key: string): string {
        // private perform translation
        const translation = key;

        if (this._translations[this.currentLang] && this._translations[this.currentLang][modul] && this._translations[this.currentLang][modul][key]) {
            return this._translations[this.currentLang][modul][key];
        }

        return translation;
    }

    instant(modul: string, key: string) {
        // call translation
        return this.translate(modul, key);
    }

}
