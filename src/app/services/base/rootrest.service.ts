import { Injectable } from '@angular/core';
import { Router} from '@angular/router';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {RootClass} from '../../models/RootClass';
import { AlertService} from '../../base-elements/services/alert.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class RootrestService extends RootClass{
  protected ServicealertService: AlertService;
  protected controller: string;
  protected cu: any;
  protected router: Router;
  protected http: Http;
  getresponse;

  constructor() {
    super();
    try {
      this.router = this.injector.get(Router);
      this.http = this.injector.get(Http);
    } catch (e) {
    }
  }

  get(action = null, params = [], auth = true, withoutmap = false, options: any = null) {
    const url = this.generateurl(action, params);
    const __response = this.http.get(url, this.setheaders(auth, options));
    if (withoutmap) return __response;
    return this.mapResponse(__response);
  }
  async syncget(action = null, params = [], auth = true, withoutmap = false, options: any = null) {
    const url = this.generateurl(action, params);
    try {
      this.getresponse = await this.http.get(url, this.setheaders(auth, options)).toPromise();//.subscribe(data => this[callback](data));
    } catch(error) {
      // Note that 'error' could be almost anything: http error, parsing error, type error in getPosts(), handling error in above code
      this.ServicealertService.error(error);
    }

      if (!withoutmap) {
        this.getresponse = this.getresponse.json();
      }
    //this.logging(this.getresponse, 'this.getresponse', 'get');
    return this.getresponse

  }
  async getDownloadpdf(action = null, params = [], download = true) {
    const pdfUrl = this.generateurl(action, params);
    const FileSaver = require('file-saver');
    const pdfName = params[0]+'.pdf';
    if (download) {
      FileSaver.saveAs(pdfUrl, pdfName);
    }
    return pdfUrl;
    /* const headerobj = { 'Accept', 'application/pdf'};
     const headers = new Headers(headerobj);
     return this.http.get(url, { headers: headers, responseType: 'blob' });*/
  }
  async getDownloadcsv(action = null, params = [], download = true) {
    const csvUrl = this.generateurl(action, params);
    ////this.logging(csvUrl, 'csvUrl', 'getDownloadcsv');
    ////this.logging(params, 'params', 'getDownloadcsv');
    const FileSaver = require('file-saver');
    const csvName = params[0]+'.csv';
    if (download) {
      FileSaver.saveAs(csvUrl, csvName);
    }
    return csvUrl;
  }
  async getDownloadXML(action = null, params = [], download = true) {
    const xmlUrl = this.generateurl(action, params);
    const FileSaver = require('file-saver');
    const XMLName = params[0]+'.xml';
    if (download) {
      FileSaver.saveAs(xmlUrl, XMLName);
    }
    return xmlUrl;
  }
  post(action = null, params = [], data: any, auth = true, callback = 'convertResponse') {
   // //this.logging('lefutittis', 'testszoveg', 'rootrest.post');
    const url = this.generateurl(action, params);
   // //this.logging(url, 'url', 'rootrest.post');
    ////this.logging(data, 'data', 'rootrest.post');
   // //this.logging(params, 'params', 'rootrest.post');
    const __response =  this.http.post(url, data, this.setheaders(auth));
    return this.mapResponse(__response, callback);
  }

  put(action = null, params = [], data: any, auth = true) {
    const url = this.generateurl(action, params);
    const __response = this.http.put(url, data, this.setheaders(auth));
    return this.mapResponse(__response);
  }

  delete(action = null, params = [], auth = true) {
    const url = this.generateurl(action, params);
    const __response = this.http.delete(url, this.setheaders(auth));
    return this.mapResponse(__response);
  }

  mapResponse(__response, callback = 'convertResponse'){
    return __response.map((response: Response) => this[callback](response)).catch(this.handleError.bind(this));
  }

  protected handleError(error: Response) {
    const data = error.json();
    if (this.constructor.name != 'AuthenticationService')
        switch (data.data) {
            case 'Token is Invalid':
            case 'Token is expired':
            case 'Something is wrong': {
                localStorage.removeItem('currentUser');
                const url = this.router.url;
                this.router.navigate(['/login'], { queryParams: { returnUrl: url }});
                break;
            }
        }
    const errors = this._.isArray(data.data) ? this._.values(data.data) : [data.data];
    return Observable.throw(errors || 'Server error');
  }

  protected convertResponse(response){
      const data = response.json();
      return data;
  }

  protected generateurl(action, params){
    let url = this.environment.apiUrl + this.controller;
    url += (this._.isNull(action)) ? '/' : '/' + action + '/';
    if (!this._.isEmpty(params)){
      url += params.join('/');
      url += '/';
    }
    url = this.__.trimEnd(url, '/');
    return url;

  }

  protected setheaders(auth = true, params: any = null){
    const headerobj = { 'Content-Type': 'application/json'};
    if (auth){
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser && this.currentUser.token) {
          headerobj['Authorization'] =  'Bearer ' + this.currentUser.token;
        }
    }
    const headers = new Headers(headerobj);
    const options = { headers: headers };
    if (!this._.isEmpty(params))
      options['params'] = params.generateParams(params);
    const reqopt = new RequestOptions(options);
    return reqopt;

  }
   async syncpost(action = null, params = [], data: any, auth = true, callback = 'convertResponse') {
    // //this.logging('lefutittis', 'testszoveg', 'rootrest.post');
    const url = this.generateurl(action, params);
    // //this.logging(url, 'url', 'rootrest.post');
    ////this.logging(data, 'data', 'rootrest.post');
    // //this.logging(params, 'params', 'rootrest.post');
     try{
      const __response = await this.http.post(url, data, this.setheaders(auth)).toPromise();
       const dmdata =__response.json();
       return dmdata.data;
       } catch(error) {
        // Note that 'error' could be almost anything: http error, parsing error, type error in getPosts(), handling error in above code
       this.ServicealertService.error(error);
        this.handleError(error);
      }



    //return this.mapResponse(__response, callback);
  }
  async syncresppost(action = null, params = [], data: any, auth = true, callback = 'convertResponse') {
    // //this.logging('lefutittis', 'testszoveg', 'rootrest.post');
    const url = this.generateurl(action, params);
    // //this.logging(url, 'url', 'rootrest.post');
    ////this.logging(data, 'data', 'rootrest.post');
    // //this.logging(params, 'params', 'rootrest.post');
    try{
      const __response = await this.http.post(url, data, this.setheaders(auth)).toPromise();
      const dmdata =__response.json();
      return dmdata;
    } catch(error) {
      // Note that 'error' could be almost anything: http error, parsing error, type error in getPosts(), handling error in above code
      this.ServicealertService.error(error);
      this.handleError(error);
    }



    //return this.mapResponse(__response, callback);
  }
}
