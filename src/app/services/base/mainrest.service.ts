
import { RootrestService } from './rootrest.service';

export class MainrestService extends RootrestService{
    constructor() {
          super();
    }

    activate(id: string) {
        return this.get('activate', [id]);
    }

    inactivate(id: string) {
        return this.get('inactivate', [id]);
    }
    storno(id: string) {
      return this.get('storno', [id]);
    }
    list(params = null) {
        //this.logging(params.apiparams, 'params', 'mainrest.list');
        let vals = [];
        let macro = '';
      this.logging(params.macrocolumn, 'params.macrocolumn', 'mainrest.list');
      this.logging(params.macrocolumn.filter.value, 'params.macrocolumn.filters.value', 'mainrest.list');
        let apiparams : {
             size: number,
             total: number,
             totalPages: number,
             pageNumber: number,
             from: number,
             to: number,
             offset : number
        } ;
        if (! this._.isNull(params)) {
          vals = [params.apiparams.page.size];
          apiparams = params.apiparams.page;
          apiparams.offset = params.apiparams.page.size*params.apiparams.page.pageNumber;
          apiparams.to = params.apiparams.page.to;
          macro = params.macrocolumn.filter.value
        }

      let data = {filters : params.columns.filters,api : apiparams, felh_userid : this.currentUser.felhasznalo.felh_userid, menuparameter: params.apiparams.menuparameter,macro: macro};
      //this.logging(data, 'data', 'mainrest.list');
        return this.post('list', [], data);
    }
    listprint(params = null) {
      ////this.logging(params.apiparams, 'params', 'mainrest.list');
      let vals = [];
      let macro = '';
      let apiparams : {
        size: number,
        total: number,
        totalPages: number,
        pageNumber: number,
        from: number,
        to: number,
        offset : number
      } ;
      if (! this._.isNull(params)) {
        vals = [params.apiparams.printpage.size];
        apiparams = params.apiparams.printpage;
        apiparams.offset = params.apiparams.printpage.size*params.apiparams.printpage.pageNumber;
        apiparams.to = params.apiparams.printpage.to;
        macro = params.macrocolumn.filter.value
      }
      return this.post('listprint', [], {filters : params.columns.filters,api : apiparams, felh_userid : this.currentUser.felhasznalo.felh_userid, menuparameter: this.menuparameter,macro: macro});
    }
    listDn(data: any = {}) {
      return this.post('list/dropdown', [], data, true);
    }

    getAll() {
      return this.get([], [], true, false);
    }

    getById(id: string) {
        return this.get(null, [id]);
    }
    async syncgetById(id: string) {
     // this.logging(id, 'id', 'mainrestsyncgetById syncgetById',true);
      let data = await this.syncget(null, [id]);

      if (data.status=='success') {
        return data.data[0];
      } else{
        return data;
      }
    }
    create(data: any) {
        return this.post(null, [], data);
    }

    update(data: any) {
        return this.put(null, [data.id], data);
    }

    destroy(id: string) {
        return this.delete(null, [id]);
    }
    runcommand(ID: string){
      return this.post('runcommand', [], {id: ID});
    }
  async synclist(params = null) {
    //this.logging(params.apiparams, 'params', 'mainrest.list');
    let vals = [];
    let macro = '';
    this.logging(params.macrocolumn, 'params.macrocolumn', 'mainrest.synclist');
    let apiparams : {
      size: number,
      total: number,
      totalPages: number,
      pageNumber: number,
      from: number,
      to: number,
      offset : number
    } ;
    if (! this._.isNull(params)) {
      vals = [params.apiparams.page.size];
      apiparams = params.apiparams.page;
      apiparams.offset = params.apiparams.page.size*params.apiparams.page.pageNumber;
      apiparams.to = params.apiparams.page.to;
      macro = params.macrocolumn.filter.value
    }
    this.logging(params.macrocolumn, 'params.macrocolumn', 'mainrest.synclist',true);
    /////this.logging(params.apiparams.menuparameter, 'params.apiparams.menuparameter', 'mainrest.list');
    let data = {filters : params.columns.filters,api : apiparams, felh_userid : this.currentUser.felhasznalo.felh_userid, menuparameter: params.apiparams.menuparameter,macro: macro};
    //this.logging(data, 'data', 'mainrest.list');
    return await this.syncresppost('list', [], data);
  }

}
