import { environment } from '../../environments/environment';
import { ExtUnderscore } from '../utility/ext-underscore';
import {TranslateService} from '@ngx-translate/core';
import { AppInjector } from '../services/custom/app.injector.service';
import { DateHelper } from '../utility/date_helper';
import { LogHelper } from '../utility/log_helper';
import { ToastrService } from "ngx-toastr";
import * as _ from 'underscore';
import * as __ from 'lodash';
import * as $ from 'jquery';

const uuidv4 = require('uuid/v4');

export class RootClass{
  uuid = uuidv4;
  TranslateService: TranslateService;
  injector: any;
  environment: any;
  currentUser: any;
  subscription: any;
  toastr: any;
  ExtUnderscore: any = ExtUnderscore;

  _: any = _;
  __: any = __;
  $: any = $;
  public menu_id;
  public menu_title;
  constructor() {
    this.environment = environment;
    this.injector = AppInjector.getInjector();
    try {
      this.TranslateService = this.injector.get(TranslateService);
    this.TranslateService.setDefaultLang('hu');

    this.TranslateService.use('hu');
    } catch (e) {
    }
     this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  getPropVal(object: any, prop: string, def: any = null){
      return this.__.get(object, prop, def);
  }

  initialDate(date){
    return new DateHelper(date);
  }

  translate(key: string): string
  {
    let translatedText = '';

    this.TranslateService.get(key).subscribe((translated: string) => {
      translatedText = translated;
    });

    return translatedText;
  }

  translateWParams(key: string, param: string = ''): string
  {
    let translatedText = '';

    this.TranslateService.get(key, {value: param}).subscribe((translated: string) => {
      translatedText = translated;
    });

    return translatedText;
  }

  logging(object: any, pname: string = '', caller: string = 'logging', isdeb: boolean = false){
     LogHelper.logging(object, pname, caller,isdeb);
  //  if (isdeb) debugger;
  }

  getGetters(): string[] {
    return Object.keys(this.constructor.prototype).filter(name => {
        return typeof Object.getOwnPropertyDescriptor(this.constructor.prototype, name)['get'] === 'function';
    });
  }

  getProperties(){
    const self       = this;
    const props      = {};
    const properties = this.getGetters();
    properties.forEach(prop => {
      if (self._.isArray(self[prop]))
      {
        props[prop] = self.__getProperties(prop);
      }
      else
      {
        props[prop] = self.__getProperty(self[prop]);
      }
    });

    return props;
  }

  __getProperties(property)
  {
    const self       = this;
    const properties = [];

    this[property].forEach(prop => {
      properties.push(self.__getProperty(prop));
    });

    return properties;
  }

  __getProperty(prop)
  {
      if (!this._.isEmpty(prop) && !this._.isNull(prop) && typeof prop.getProperties === 'function')
      {
        return prop.getProperties();
      }
      else
      {
        return prop;
      }
  }

  getJsonProperty(json: string, prop: string)
  {
    const obj = JSON.parse(json);
    return this.getPropVal(obj, prop, null);
  }
  showToastrInfo(message) {
    this.toastr = this.injector.get(ToastrService);
    this.toastr.info(
      message,
      undefined,
      {
        closeButton: true,
        positionClass: "toast-top-right"
      }
    );
  }
}
